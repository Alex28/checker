package test.checker;

import static org.junit.Assert.assertEquals;
import test.checker.FileReader;

import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Parameter;
import ru.yandex.qatools.allure.annotations.Title;

@RunWith(Parameterized.class)

@Title("Test Suite")
@Description("We will check results of operations in this test suite")
public class CheckerTest {
	@Parameter("Operand 1")
	private String operandA;
	@Parameter("Operand 2")
	private String operandB;
	@Parameter("Operation")
	private String operator;
	@Parameter("Expected Result")
	private String expectedResult;

	// Конструктор класса
	public CheckerTest(String operandA, String operandB, String operator, String expectedResult) {
		this.operandA = operandA;
		this.operandB = operandB;
		this.operator = operator;
		this.expectedResult = expectedResult;
	}

	// Параметры для конструктора класса
	@Parameters(name = "№{index}: {0}{2}{1}={3}")
	public static List<String[]> dataParameters() {
		FileReader fileReader = new FileReader();
		List<String> lines = fileReader.readFromFile();
		String[][] array = new String[lines.size()][4];
		String oneLine;
		String[] lineArray;
		for (int i = 0; i < lines.size(); i++) {
			oneLine = lines.get(i);
			lineArray = oneLine.split(";");
			System.out.println(i);
			for (int j = 0; j < lineArray.length; j++) {
				array[i][j] = lineArray[j];
			}
		}
		return Arrays.asList(array);
	}

	@Title("Check Operation")
	@Description("We check the results of operation in this test case")
	@Test
	public void checkResultOfOperation() {

		int operand1 = Integer.parseInt(operandA);
		int operand2 = Integer.parseInt(operandB);
		int resuntExpected = Integer.parseInt(expectedResult);
		int resultActual;
		if (operator.equals("+")) {
			resultActual = operand1 + operand2;
		} else if (operator.equals("-")) {
			resultActual = operand1 - operand2;
		} else if (operator.equals("*")) {
			resultActual = operand1 * operand2;
		} else {
			resultActual = operand1 / operand2;
		}
		assertEquals("Ошибка проверки операции " + operator + " :", resuntExpected, resultActual);
	}
}