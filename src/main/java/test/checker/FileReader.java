package test.checker;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;

public class FileReader {

	public List<String> readFromFile() {
		List<String> lines;
		try {
			File file = new File("src/test/resources/DateFile.txt");
			lines = FileUtils.readLines(file, "utf-8");
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return lines;
	}
}