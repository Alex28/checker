## Java Test Task

### Getting Started

To generate Allure Report you should perform following steps:

$ mvn clean test
$ mvn site

To see a report, open '/target/site/allure-maven-plugin/index.html' in your browser